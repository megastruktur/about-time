import {DTCalc} from "./calendar/DTCalc";
import {DTMod} from "./calendar/DTMod";

const _moduleSocket = "module.about-time";
const _updateClock = "about-time.updateClock";
const _eventTrigger = "about-time.eventTrigger";
const _queryMaster = "about-time.queryMaster";
const _masterResponse = "about-time.masterResponse"
const _masterMutiny = "about-time.Arrrgh...Matey";
const _runningClock = "about-time.clockRunningStatus"
const _acquiredMaster = "about-time.pseudoclockMaster"
export const _showTimer = "about-time.showTimer"

let _userId: string = "";

let debug: Boolean;

let log = (...args) => {
  console.log("about-time | ", ...args);
}

export class PseudoClockMessage {
  _action: string;
  _userId: string;
  _newTime: number;
  _args: any[];

  constructor({action, userId, newTime=0}, ...args) {
    this._action = action;
    this._userId = userId;
    this._newTime = newTime;
    this._args = args;
    return this;
  }
}

export class PseudoClock {
  static _currentTime: number; // time in seconds
  static get currentTime() {return PseudoClock._currentTime};
  private static _realTimeMult: number;
  private static _running: Boolean;
  private static _realTimeInteval: number;

  private static _isMaster: Boolean;
  private static _realTimeTimerID: NodeJS.Timeout;
  private static _queryTimeoutId: NodeJS.Timeout;
  private static _saveInterval = 1 * 60 * 1000; // only save every 1 minutes real time. make a param.
  private static _clockStartYear = 1;
  private static _isGM;
  static _globalRunning;
  static get clockStartYear () { return this._clockStartYear};
  static setDebug(val: boolean) {
    debug = val;
  }

  static isGM() { return PseudoClock._isGM}
  
  static _initialize(currentTime: number = 0, realTimeMult: number = 0,realTimeInteval: number, running: Boolean = false) {
    PseudoClock._currentTime = currentTime;
    PseudoClock._running = running;
    PseudoClock._globalRunning = running;
    PseudoClock._realTimeMult = realTimeMult;
    PseudoClock._realTimeInteval = realTimeInteval;
    PseudoClock._save(true);
  }

  static _createFromData(data: any) {
    PseudoClock._currentTime = data._currentTime || 0;
    PseudoClock._running = data._running;
    PseudoClock._globalRunning = data._running; 
    PseudoClock._realTimeMult = data._realTimeMult;
    PseudoClock._realTimeInteval = data._realTimeInteval;
    PseudoClock.setClock(PseudoClock._currentTime);
  }

  static isMaster(): Boolean {
    return PseudoClock._isMaster;
  }

  static warnNotMaster(operation) {
    ui.notifications.error(`${game.user.name} ${operation} - ${game.i18n.localize ("about-time.notMaster")}`);
    console.warn(`Non master timekeeper attempting to ${operation}`)
  }

  static warnNotGM(operation) {
    ui.notifications.error(`${game.user.name} ${operation} - ${game.i18n.localize ("about-time.notMaster")}`);
    console.warn(`Non GM attempting to ${operation}`);
  }

  static status() {
    console.log(
      PseudoClock._currentTime,
      PseudoClock._realTimeMult,
      PseudoClock._running,
      PseudoClock._globalRunning
    )
  }

  static _displayCurrentTime() {
    console.log(`Elapsed time ${PseudoClock._currentTime}`);
  }

  static getDHMS(): DTMod {
    return DTMod.fromSeconds(this._currentTime)
  }
  
  static advanceClock(timeIncrement: number) {
    if (PseudoClock._isGM)
      PseudoClock.setClock(PseudoClock._currentTime + timeIncrement);
    else 
      PseudoClock.warnNotGM("Advance clock");
  }

  static setClock(newTime: number) {
    // PseudoClock._currentTime = newTime;
    if (debug) log("pseudoClock setting time to ", PseudoClock._currentTime);

    if (PseudoClock._isGM) {
      let message = new PseudoClockMessage({action: _updateClock, userId: _userId, newTime: newTime});
      PseudoClock._notifyUsers(message);
      PseudoClock._save(false);
     }
    Hooks.callAll("pseudoclockSet", newTime);
  }

  static _realTimeHandler() {
    if (debug) log("Real time handler fired");
    // reschedule the handleer
    PseudoClock._realTimeTimerID = setTimeout(PseudoClock._realTimeHandler, PseudoClock._realTimeInteval);
    if (!game.paused && PseudoClock._running) {
      let gameTimeAdvance = PseudoClock._realTimeMult * PseudoClock._realTimeInteval / 1000;
      PseudoClock.advanceClock(gameTimeAdvance);
    }
  }

  static demote() {
    PseudoClock._isMaster = false;
    Hooks.callAll(_acquiredMaster, false);
    clearTimeout(PseudoClock._realTimeTimerID);
  }

  public static showTimer(duration: {}) {
    let message = new PseudoClockMessage({action: _showTimer, userId: _userId}, duration)
    PseudoClock._notifyUsers(message);
  }
  static notifyMutiny() {
    let message = new PseudoClockMessage({action: _masterMutiny, userId: _userId})
    PseudoClock._notifyUsers(message);
  }
  static mutiny() {
    PseudoClock.notifyMutiny();
    let timeout = game.settings.get("about-time", "election-timeout") / 2 * 1000;
    // 2 set a timeout, if it expires assume master timekeeper role.
    PseudoClock._queryTimeoutId = setTimeout(() => {
      log("Mutineer assuming master timekeeper role")
      PseudoClock._isMaster = true;
      PseudoClock._load();
      clearTimeout(PseudoClock._realTimeTimerID);
      PseudoClock._realTimeTimerID = setTimeout(PseudoClock._realTimeHandler, PseudoClock._realTimeInteval);
      Hooks.callAll(_acquiredMaster, true);
      let message = new PseudoClockMessage({action: _masterResponse, userId: _userId, newTime: PseudoClock._currentTime});
      PseudoClock._notifyUsers(message);
    }, timeout);

  }

  static notifyRunning(status: boolean) {
    let message = new PseudoClockMessage({action: _runningClock, userId: _userId}, status)
    PseudoClock._notifyUsers(message);
    Hooks.callAll(_runningClock, status);
  }

  /* Start the real time clock */
  static startRealTime() {
    if (PseudoClock._isMaster) {
      clearTimeout(PseudoClock._realTimeTimerID);
      PseudoClock._realTimeTimerID = setTimeout(PseudoClock._realTimeHandler, PseudoClock._realTimeInteval);
      PseudoClock._running = true;
      PseudoClock._globalRunning = true;
      PseudoClock.notifyRunning(true);
    } else PseudoClock.warnNotMaster("Start realtime");
  }

  static stopRealTime() {
    if (PseudoClock.isMaster) {
      PseudoClock._running = false;
      PseudoClock._globalRunning = false;
      PseudoClock.notifyRunning(false);
    }
    else PseudoClock.warnNotMaster("Stop realtime");
  }

  static isRunning(): Boolean {
    return PseudoClock._globalRunning;
  }

  static _processAction(message: PseudoClockMessage) {
    if (message._userId === _userId) return;
    switch (message._action) {
      case _updateClock:
        PseudoClock._currentTime = message._newTime;
        Hooks.callAll("pseudoclockSet", message._newTime);
        break;
      case _eventTrigger:
        Hooks.callAll(_eventTrigger, ...message._args);
        break;
      case _queryMaster:
        if (PseudoClock._isMaster) {
          log(game.user.name, "responding as master time keeper")
          let message = new PseudoClockMessage({action: _masterResponse, userId: _userId, newTime: PseudoClock._currentTime});
          PseudoClock._notifyUsers(message);
        }
        break;
      case _masterResponse:
        if (message._userId !== _userId) {
          // cancel timeout
          clearTimeout(PseudoClock._queryTimeoutId);
          console.log("Master response message ", message)
          //@ts-ignore
          let userName = game.users.entities.find(u=>u._id === message._userId).name;
          log(userName, " as master timekeeper responded cancelling timeout")
          PseudoClock._currentTime = message._newTime;
          Hooks.callAll("pseudoclockSet", message._newTime);
        }
        break;
        case _masterMutiny:
          if (message._userId !== _userId && PseudoClock._isMaster) {
           PseudoClock.demote();
           //@ts-ignore
           let userName = game.users.entities.find(u=>u._id === message._userId).name;
           log(userName, " took control as master timekeeper. Aaaahhhrrr");
         }
         break;
        case _runningClock:
          PseudoClock._globalRunning = message._args[0];
          Hooks.callAll(_runningClock);
          break;
        case _showTimer:
          Hooks.callAll(_showTimer, message._args[0])
    }
  };

  static async  notifyEvent(eventName: string, ...args: any[]) {
    let message = new PseudoClockMessage({action: _eventTrigger, userId: _userId, newTime: 0}, eventName, ...args)
    Hooks.callAll(_eventTrigger, ...message._args);
    return PseudoClock._notifyUsers(message);
  }

  static async _notifyUsers(message: PseudoClockMessage) {
    //@ts-ignore
        await game.socket.emit(_moduleSocket, message, resp => {
    });
  }

  static _setupSocket() {
    //@ts-ignore
    game.socket.on(_moduleSocket, (data: ElapsedTimeMessage) => {
      PseudoClock._processAction(data);
    });
  };

  static _load() {
    let saveData = game.settings.get("about-time", "pseudoclock");
    if (debug) log("_load", saveData);
    try {
      if (!saveData) {
        if (debug) log("no saved data re-initializing");
        PseudoClock._initialize(0, 0, 30, false);
      } else {
        if (debug) log("loaded saved Data. ", saveData);
        PseudoClock._createFromData(saveData);
      }
    } catch (err) {
      console.log(err);
      PseudoClock._initialize(0, 0, 30, false)
    }
    PseudoClock._fetchParams();
  }

  static _lastSaveTime;;

  static _save(force: boolean) {
    let newSaveTime = Date.now();
    if (PseudoClock._isMaster) {
      if (debug) log("save times are ", newSaveTime, PseudoClock._lastSaveTime, PseudoClock._saveInterval);
      if ((newSaveTime - PseudoClock._lastSaveTime > PseudoClock._saveInterval) || force) {
        if (debug) log("_save saving", new Date(), PseudoClock.currentTime);
        let saveData = { 
          _currentTime: PseudoClock._currentTime,
          _running: PseudoClock._running,
          _realTimeInteval: PseudoClock._realTimeInteval,
          _realTimeMult: PseudoClock._realTimeMult
        }
        // put something in to throttle saving
        game.settings.set("about-time", "pseudoclock", saveData);
        PseudoClock._lastSaveTime = newSaveTime;
      }
    }
  }

  static clockSetHandler = (newTime) => {
    PseudoClock._currentTime = newTime;
  }

  static init() {
    _userId = game.user.id;
    PseudoClock._isGM = game.user.isGM;
    PseudoClock._lastSaveTime = Date.now();
    PseudoClock._fetchParams();

    //@ts-ignore
    Hooks._hooks.pseudoclockSet = [PseudoClock.clockSetHandler].concat(Hooks._hooks.pseudoclockSet || []);
    // find a better way to do this.
    PseudoClock._isMaster = false;
    PseudoClock._setupSocket();
    // 1 send a message to see if there is another master clock already out there
    if (debug) log("pseudoclock sending query master message")
    let message = new PseudoClockMessage({action: _queryMaster, userId: _userId});
    PseudoClock._notifyUsers(message);
    if (PseudoClock._isGM) {
      let timeout = game.settings.get("about-time", "election-timeout") * 1000;
      // 2 set a timeout, if it expires assume master timekeeper role.
      PseudoClock._queryTimeoutId = setTimeout(() => {
        log("Assuming master timekeeper role")
        PseudoClock.notifyMutiny();
        PseudoClock._isMaster = true;
        PseudoClock._load();
        clearTimeout(PseudoClock._realTimeTimerID);
        PseudoClock._realTimeTimerID = setTimeout(PseudoClock._realTimeHandler, PseudoClock._realTimeInteval);
        Hooks.callAll(_acquiredMaster, true);
      }, timeout);
    }
    if (debug) log("election-timeout: timeout set id is ", PseudoClock._queryTimeoutId);
  }
  static _fetchParams() {
    PseudoClock._realTimeMult = game.settings.get("about-time", "real-time-multiplier");
    if (isNaN(PseudoClock._realTimeMult)) PseudoClock._realTimeMult = 1;
    PseudoClock._realTimeInteval = (game.settings.get("about-time", "real-time-interval") || 15) * 1000;
  }
}

